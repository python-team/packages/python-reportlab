Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Files-Excluded: src/reportlab.egg-info/*
Upstream-Name: ReportLab
Upstream-Contact: ReportLab Inc. <info@reportlab.com>
Source: http://www.reportlab.com

Files: *
Copyright: 2000-2014, ReportLab Inc. <info@reportlab.com>
License: ReportLab-License

Files: src/reportlab/fonts/DarkGarden*
Copyright: 1999, 2000, 2004 Michal Kosmulski <mkosmul@users.sourceforge.net>
License: GPL-2-with-exception

Files: src/reportlab/fonts/Vera*
Copyright: 2003 Bitstream, Inc.
License: Bitstream-License

Files: src/reportlab/fonts/callig15*
Copyright: 1992 Peter Vanroose
	   1999 S. Dachian
License: Vanroose-License

Files: debian/*
Copyright: 2000-2003 Gregor Hoffleit <flight@debian.org>
	   2023-2024 Georges Khaznadar <georgesk@debian.org>
License: GPL-2+

License: ReportLab-License
 Copyright (c) 2000-2008, ReportLab Inc.
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer. 
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution. 
  * Neither the name of the company nor the names of its contributors may be
    used to endorse or promote products derived from this software without
    specific prior written permission. 
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE OFFICERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: GPL-2-with-exception
 This font is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This font is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this font; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 .
 As a special exception, if you create a document which uses
 this font, and embed this font or unaltered portions of this font into
 the document, this font does not by itself cause the resulting
 document to be covered by the GNU General Public License.  This
 exception does not however invalidate any other reasons why the
 document might be covered by the GNU General Public License.  If you
 modify this font, you may extend this exception to your version of the
 font, but you are not obligated to do so. If you do not wish to do so,
 delete this exception statement from your version.

Comment: Bitstream Vera Fonts Copyright
 The fonts have a generous copyright, allowing derivative works (as
 long as "Bitstream" or "Vera" are not in the names), and full
 redistribution (so long as they are not *sold* by themselves). They
 can be be bundled, redistributed and sold with any software.

License: Bitstream-License
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of the fonts accompanying this license ("Fonts") and associated
 documentation files (the "Font Software"), to reproduce and distribute
 the Font Software, including without limitation the rights to use,
 copy, merge, publish, distribute, and/or sell copies of the Font
 Software, and to permit persons to whom the Font Software is furnished
 to do so, subject to the following conditions:
 .
 The above copyright and trademark notices and this permission notice
 shall be included in all copies of one or more of the Font Software
 typefaces.
 .
 The Font Software may be modified, altered, or added to, and in
 particular the designs of glyphs or characters in the Fonts may be
 modified and additional glyphs or characters may be added to the
 Fonts, only if the fonts are renamed to names not containing either
 the words "Bitstream" or the word "Vera".
 .
 This License becomes null and void to the extent applicable to Fonts
 or Font Software that has been modified and is distributed under the
 "Bitstream Vera" names.
 .
 The Font Software may be sold as part of a larger software package but
 no copy of one or more of the Font Software typefaces may be sold by
 itself.
 .
 THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL
 BITSTREAM OR THE GNOME FOUNDATION BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF THE USE OR INABILITY TO USE THE FONT
 SOFTWARE OR FROM OTHER DEALINGS IN THE FONT SOFTWARE.
 .
 Except as contained in this notice, the names of Gnome, the Gnome
 Foundation, and Bitstream Inc., shall not be used in advertising or
 otherwise to promote the sale, use or other dealings in this Font
 Software without prior written authorization from the Gnome Foundation
 or Bitstream Inc., respectively. For further information, contact:
 fonts at gnome dot org.

License: Vanroose-License
 You may freely use, modify and/or distribute this file, as long as
 this copyright notice is retained

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".